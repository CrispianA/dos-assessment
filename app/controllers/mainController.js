var app = angular.module('retention-app', ['ngRoute']);

app.config(function ($routeProvider) { // configure routes for navigation
  $routeProvider

    .when('/home', {
    templateUrl: 'views/home/home.html',
    controller: 'mainController'
  })

  .when('/about', {
    templateUrl: 'views/about/about.html',
    controller: 'aboutController'
  });

});

app.controller('mainController', ['$scope', '$http', function ($scope, $http, jsonUpdate) {
  $scope.homeMessage = "This is home...";

  $http.get('../app/js/{be_creative}.json').success(function (data) {
    // get the data from the json file for Carousel images to load up in the view
    $scope.Carousel = data.Carousel;
  });
  
  $scope.getJSON = function() {
    jsonService.getJSON()
    .then(function (response) {
      $scope.data = response.data;
    });
  };
  
  $scope.uploadSlide = function(newSlide) {
    $scope.Carousel.push({newSlide});
  };

}]);

app.controller('aboutController', ['$scope', '$http', function ($scope, $http) {

  /* 
     Here I make use of a get request to pass data from 
     json file to the About Us page 
  */

  $http.get('../app/js/{be_creative}.json').success(function (data) {
    $scope.AboutUs = data.AboutUs;
  });

}]);
var app = angular.module('retention-app', []);

app.service('jsonService', function($http) {
  return {
    getJSON: function() {
      return $http.get('../app/js/{be_creative}.json');
    }
  };
});